<?php
$title = "Übersicht";
$navoverview = "";

include './frames/header.php';
include './inc/checkuser.php';
include './inc/config.php';

if (filter_has_var(INPUT_GET, "del") && filter_input(INPUT_GET, "del") == true) {
    $queryDelete = "DELETE FROM `edith_stein_schoolprogram_db`.`sp_toc` WHERE (`id` = '" . filter_input(INPUT_GET, "idtoc") . "');";
    $db_schoolprogram->query($queryDelete);
}

if (filter_has_var(INPUT_POST, "add")) {
    $queryAdd = "INSERT INTO `edith_stein_schoolprogram_db`.`sp_toc` (`item`, `level_1`, `level_2`, `level_3`, `level_4`, `level_5`) VALUES ("
            . "'" . filter_input(INPUT_POST, "item") . "', "
            . "'" . filter_input(INPUT_POST, "level_1") . "', "
            . "'" . filter_input(INPUT_POST, "level_2") . "', "
            . "'" . filter_input(INPUT_POST, "level_3") . "', "
            . "'0', "
            . "'0');";
    $db_schoolprogram->query($queryAdd);
}

if (filter_has_var(INPUT_POST, "idcontent")) {
    if (empty(filter_input(INPUT_POST, "idcontent"))) {
        //add new data set
        $queryAdd = "INSERT INTO `edith_stein_schoolprogram_db`.`sp_content` (`id_toc`, `content`, `lastmodified`) VALUES ("
                . "'" . filter_input(INPUT_POST, "idtoc") . "', "
                . "'" . filter_input(INPUT_POST, "content") . "', "
                . "'" . date("Y-m-d H:i:s") . "'"
                . ");";
        $db_schoolprogram->query($queryAdd);
    } else {
        // edit data set
        $queryUpdate = "UPDATE `edith_stein_schoolprogram_db`.`sp_content` SET `content` = '" . filter_input(INPUT_POST, "content") . "', lastmodified = '" . date("Y-m-d H:i:s") . "' WHERE (`id` = '" . filter_input(INPUT_POST, "idcontent") . "');";
        $db_schoolprogram->query($queryUpdate);
    }
}

if (filter_has_var(INPUT_GET, "id")) {
    $queryContent = "SELECT * FROM edith_stein_schoolprogram_db.sp_content WHERE id_toc = '" . filter_input(INPUT_GET, "id") . "';";
    $resultContent = $db_schoolprogram->query($queryContent);
    $rowContent = $resultContent->fetch_array(MYSQLI_ASSOC);
}

$queryTOC = "SELECT * FROM edith_stein_schoolprogram_db.sp_toc ORDER BY level_1, level_2, level_3;";
$resultTOC = $db_schoolprogram->query($queryTOC);

$queryLevel1 = "SELECT * FROM edith_stein_schoolprogram_db.sp_toc WHERE level_2 = 0 AND level_3 = 0 AND level_4 = 0 AND level_5 = 0 ORDER BY level_1;";
$resultLevel1 = $db_schoolprogram->query($queryLevel1);
?>
<div class="container-fluid">
    <?php
    if (isset($message)) {
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-<?= $alert; ?>">
                    <?= $message; ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="row mt-2">
        <div class="col-md-4">
            <!-- Button trigger modal -->
            <div class="btn-group">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editTOC">
                    <i class="fas fa-edit"></i>
                </button>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addTOC">
                    <i class="fas fa-plus"></i>
                </button>
            </div>
            <!-- Modal edit -->
            <div class="modal fade" id="editTOC" tabindex="-1">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Kapitel-Überschriften bearbeiten</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info">Die Tabellenzeilen sind wie eine Nummerierung mit zwei Unterebenen zu lesen, z. B. "2.1 Erzieherisches Leitbild" oder "3.1.2 Konkrete Umsetzung".</div>
                            <div class="alert alert-danger">
                                Beim Löschen eines Eintrages bitte beachten:
                                <ul>
                                    <li>Das Löschen kann nicht rückgängig gemacht werden.</li>
                                    <li>Der zugehörige Text kann danach nicht mehr aufgerufen werden. Gegebenenfalls muss der Text <strong>vor</strong> dem Löschen einer Kapitelüberschrift einem anderen Abschnitt zugeordnet werden.</li>
                                </ul>
                            </div>
                            <form method="post" action="<?= filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_URL); ?>?id=<?= filter_input(INPUT_GET, "id"); ?>">
                                <table class="table">
                                    <colgroup>
                                        <col style="width: 15%" />
                                        <col style="width: 15%" />
                                        <col style="width: 15%" />
                                        <col style="width: 50%" />
                                        <col style="width: 5%" />
                                    </colgroup>
                                    <tr>
                                        <th>h1</th>
                                        <th>h2</th>
                                        <th>h3</th>
                                        <th>Bezeichnung</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    <?php
                                    while ($rowTOC = $resultTOC->fetch_array(MYSQLI_ASSOC)) {
                                        ?>
                                        <tr>
                                            <td><input type="number" min="0" class="form-control" name="level_1" value="<?= $rowTOC['level_1']; ?>"></td>
                                            <td><input type="number" min="0" class="form-control" name="level_2" value="<?= $rowTOC['level_2']; ?>"></td>
                                            <td><input type="number" min="0" class="form-control" name="level_3" value="<?= $rowTOC['level_3']; ?>"></td>
                                            <td><input type="text" class="form-control" name="item" value="<?= $rowTOC['item']; ?>"></td>
                                            <td><a href="<?= filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_URL); ?>?id=<?= filter_input(INPUT_GET, "id"); ?>&amp;idtoc=<?= $rowTOC['id']; ?>&amp;del=true" class="btn btn-danger delTOC"><i class="fas fa-times"></i></a></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                            <button type="button" class="btn btn-primary" name="edit">Speichern</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--modal add-->
            <div class="modal fade" id="addTOC" tabindex="-1">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <form method="post" action="<?= filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_URL); ?>?id=<?= filter_input(INPUT_GET, "id"); ?>">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Kapitel-Überschrift hinzufügen</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-info">Die Zeile ist wie eine Nummerierung mit zwei Unterebenen zu lesen, z. B. "2.1 Erzieherisches Leitbild" oder "3.1.2 Konkrete Umsetzung".</div>
                                <table class="table">
                                    <colgroup>
                                        <col style="width: 15%" />
                                        <col style="width: 15%" />
                                        <col style="width: 15%" />
                                        <col style="width: 55%" />
                                    </colgroup>
                                    <tr>
                                        <th>h1</th>
                                        <th>h2</th>
                                        <th>h3</th>
                                        <th>Bezeichnung</th>
                                    </tr>
                                    <tr>
                                        <td><input type="number" min="0" class="form-control" name="level_1" required="required" autofocus="autofocus"></td>
                                        <td><input type="number" min="0" class="form-control" name="level_2" required="required"></td>
                                        <td><input type="number" min="0" class="form-control" name="level_3" required="required"></td>
                                        <td><input type="text" class="form-control" name="item" placeholder="Überschrift des Kapitels" required="required"></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                                <button type="submit" class="btn btn-primary" name="add">Speichern</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <hr />
            <ol>
                <?php
                while ($rowLevel1 = $resultLevel1->fetch_array(MYSQLI_ASSOC)) {
                    $queryLevel2 = "SELECT * FROM edith_stein_schoolprogram_db.sp_toc WHERE level_1 = '" . $rowLevel1['level_1'] . "' AND level_2 > 0 AND level_3 = 0 AND level_4 = 0 AND level_5 = 0 ORDER BY level_2;";
                    $resultLevel2 = $db_schoolprogram->query($queryLevel2);
                    ?>
                    <li>
                        <?= $rowLevel1['item']; ?>
                        <ol>
                            <?php
                            while ($rowLevel2 = $resultLevel2->fetch_array(MYSQLI_ASSOC)) {
                                $queryLevel3 = "SELECT * FROM edith_stein_schoolprogram_db.sp_toc WHERE level_1 = '" . $rowLevel1['level_1'] . "' AND level_2 = '" . $rowLevel2['level_2'] . "' AND level_3 > 0  AND level_4 = 0 AND level_5 = 0 ORDER BY level_3;";
                                $resultLevel3 = $db_schoolprogram->query($queryLevel3);
                                ?>
                                <li>
                                    <a href="<?= filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_URL); ?>?id=<?= $rowLevel2['id']; ?>" class="<?= ($rowLevel2['id'] == filter_input(INPUT_GET, "id")) ? " btn btn-sm btn-primary" : " btn btn-sm btn-link"; ?>"><?= $rowLevel2['item']; ?></a>
                                    <ol>
                                        <?php
                                        while ($rowLevel3 = $resultLevel3->fetch_array(MYSQLI_ASSOC)) {
                                            ?>
                                            <li>
                                                <a href="<?= filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_URL); ?>?id=<?= $rowLevel3['id']; ?>" class="<?= ($rowLevel3['id'] == filter_input(INPUT_GET, "id")) ? " btn btn-sm btn-primary" : " btn btn-sm btn-link"; ?>"><?= $rowLevel3['item']; ?></a>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ol>
                                </li>
                                <?php
                            }
                            ?>
                        </ol>
                    </li>
                    <?php
                }
                ?>
            </ol>
        </div>
        <div class="col-md-8">
            <form method="post" action="<?= filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_URL); ?>?id=<?= filter_input(INPUT_GET, "id"); ?>">
                <?php
                if (filter_has_var(INPUT_GET, "id") && !empty(filter_input(INPUT_GET, "id"))) {
                    ?>
                    <small class="text-muted">
                        <input type="text" readonly class="form-control-plaintext" value="zuletzt geändert am <?= date_format(date_create($rowContent['lastmodified']), "d.m.Y H:i"); ?>" />
                    </small>
                    <textarea id="content" name="content">
                        <?= $rowContent['content']; ?>
                    </textarea>
                    <input type="hidden" name="idcontent" value="<?= $rowContent['id']; ?>">
                    <input type="hidden" name="idtoc" value="<?= filter_input(INPUT_GET, "id"); ?>">
                    <button type="submit" name="submit" class="btn btn-primary mt-2">Speichern</button>
                    <?php
                }
                ?>
            </form>
        </div>
    </div>
</div>
<?php
include './frames/footer.php';
