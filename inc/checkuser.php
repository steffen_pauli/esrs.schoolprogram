<?php

if (!isset($_SESSION)) {
    session_start();
}
/*
 * Copyright (C) 2018 sp
 */
if (!isset($_SESSION['iduser']) || $_SESSION['role'] == 0) {
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    header('Location:index.php');
} 