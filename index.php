<?php
$title = "Home";

include './frames/header.php';
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h1>Schulprogramm</h1>
            </div>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-md-12">
            <?php
            if (!isset($_SESSION['iduser'])) {
                ?>
                <div class="alert alert-danger">
                    Bitte einloggen.
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<?php
include './frames/footer.php';
