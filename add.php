<?php
$title = "Eintragungen";
$navadd = "";

include './frames/header.php';
include './inc/checkuser.php';
include './inc/config.php';

include './isbn-master/src/Isbn/Check.php';
include './isbn-master/src/Isbn/CheckDigit.php';
include './isbn-master/src/Isbn/Exception.php';
include './isbn-master/src/Isbn/Hyphens.php';
include './isbn-master/src/Isbn/Isbn.php';
include './isbn-master/src/Isbn/Translate.php';
include './isbn-master/src/Isbn/Validation.php';
$isbn = new Isbn\Isbn();


if (filter_has_var(INPUT_POST, "add")) {
    $isbnnbr = filter_input(INPUT_POST, "isbn1") . "-" . filter_input(INPUT_POST, "isbn2") . "-" . filter_input(INPUT_POST, "isbn3") . "-" . filter_input(INPUT_POST, "isbn4") . "-" . filter_input(INPUT_POST, "isbn5");
    if ($isbn->validation->isbn($isbnnbr) == 1) {
        $queryAdd = "INSERT INTO `edith_stein_book_db`.`entries` (`count`, `isbn`, `title`, `price`, `new`, `added`, `user`, `faculty`, `year`) VALUES ("
                . "'" . filter_input(INPUT_POST, "count") . "', "
                . "'" . $isbnnbr . "', "
                . "'" . filter_input(INPUT_POST, "title") . "', "
                . "'" . filter_input(INPUT_POST, "price") . "', "
                . "'" . filter_input(INPUT_POST, "new") . "', "
                . "'" . date("Y-m-d H:m:i") . "', "
                . "'" . $_SESSION['iduser'] . "', "
                . "'" . filter_input(INPUT_POST, "faculty") . "', "
                . "'" . $year . "'"
                . ");";
        $db_book->query($queryAdd);
        $message = "Der Datensatz wurde erfolgreich hinzugefügt.";
        $alert = "success";
    } else {
        $message = "Der Datensatz wurde nicht hinzugefügt. Bitte die ISBN-Nummer (" . $isbnnbr . ") überprüfen!";
        $alert = "danger";
    }
}

if (filter_has_var(INPUT_POST, "edit")) {
//    $queryEdit = "UPDATE `edith_stein_book_db`.`entries` SET "
//            . "`count` = '12', "
//            . "`isbn` = '978-3-12-741020-3', "
//            . "`title` = 'Test2', "
//            . "`price` = '5.54', "
//            . "`added` = '2020-05-18 16:05:07', "
//            . "`user` = '2', "
//            . "`faculty` = 'E' "
//            . "WHERE (`id` = '2');";
//    $db_book->query($queryEdit);
}

if (filter_has_var(INPUT_POST, "delete")) {
    
}

$query = "SELECT * FROM edith_stein_book_db.entries E "
        . "LEFT JOIN edith_stein_book_db.user U ON E.user = U.iduser "
        . "WHERE E.year = '" . $year . "'"
        . "ORDER BY E.faculty;";
$result = $db_book->query($query);
?>
<div class="container-fluid">
    <?php
    if (isset($message)) {
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-<?= $alert; ?>">
                    <?= $message; ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="row mt-2">
        <div class="col-md-12">
            <h2>Eintragungen <small class="float-right"><a href="#" class="btn btn-success mr-3" data-toggle="modal" data-target="#addEntry"><i class="fas fa-plus"></i></a></small></h2>
            <form action="<?= filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_URL); ?>" method="post">
                <div class="modal fade text-left" id="addEntry" tabindex="-1">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="title">Datensatz hinzufügen</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-warning">Alle Angaben sind Pflichtangaben!</div>
                                <div class="form-group row">
                                    <label for="count" class="col-sm-3 col-form-label">Anzahl</label>
                                    <div class="col-sm-9">
                                        <input type="number" min="1" step="1" class="form-control" id="count" name="count" required="required">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="isbn1" class="col-sm-3 col-form-label">ISBN-13</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="isbn1" name="isbn1" maxlength="3" required="required">
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" id="isbn2" name="isbn2" maxlength="1" required="required">
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="isbn3" name="isbn3" maxlength="2" required="required">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="isbn4" name="isbn4" maxlength="6" required="required">
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" id="isbn5" name="isbn5" maxlength="1" required="required">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="title" class="col-sm-3 col-form-label">Titel</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="title" name="title" required="required">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="faculty" class="col-sm-3 col-form-label">Fachschaft</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="faculty" required="required">
                                            <option></option>
                                            <optgroup label="Hauptfächer">
                                                <option>D</option>
                                                <option>M</option>
                                                <option>E</option>
                                            </optgroup>
                                            <optgroup label="Naturwissenschaften">
                                                <option>CH</option>
                                                <option>PH</option>
                                                <option>BI</option>
                                            </optgroup>
                                            <optgroup label="Gesellschaftswissenschaften">
                                                <option>GE</option>
                                                <option>EK</option>
                                                <option>KR/ER</option>
                                                <option>PPH</option>
                                                <option>PK</option>
                                                <option>WI</option>
                                            </optgroup>
                                            <optgroup label="Sport">
                                                <option>SP</option>
                                            </optgroup>
                                            <optgroup label="Künstlerische Fächer">
                                                <option>KU</option>
                                                <option>MU</option>
                                                <option>TX</option>
                                            </optgroup>
                                            <optgroup label="Differenzierung">
                                                <option>WP BI</option>
                                                <option>WP IN</option>
                                                <option>WP TC</option>
                                                <option>WP SW</option>
                                                <option>WP F</option>
                                                <option>WP KU</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="price" class="col-sm-3 col-form-label">Preis</label>
                                    <div class="col-sm-9">
                                        <input type="number" min="1" step="0.01" class="form-control" id="price" name="price" required="required">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="new" class="col-sm-3 col-form-label">Art der Anschaffung</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="new" required="required">
                                            <option value="2">Neuanschaffung</option>
                                            <option value="1">Fortführung</option>
                                            <option value="0">Nachbestellung</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                                <button type="submit" class="btn btn-primary" name="add">Hinzufügen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-md-12">
            <table class="table entries">
                <thead>
                    <tr>
                        <th class="text-left">Anzahl</th>
                        <th class="text-left">ISBN</th>
                        <th class="text-left">Titel</th>
                        <th class="text-left">Fachschaft</th>
                        <th class="text-left">Preis</th>
                        <th class="text-left">Art der <br />Anschaffung</th>
                        <th class="text-left">erstellt/bearbeitet</th>
                        <!--<th>&nbsp;</th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php
                    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                        switch ($row['new']) {
                            case 0:
                                $new = "Nachbestellung";
                                break;
                            case 1:
                                $new = "Fortführung";
                                break;
                            case 2:
                                $new = "Neuanschaffung";
                                break;
                            default:
                                $new = "N/N";
                                break;
                        }
                        ?>
                        <tr>
                            <td><?= $row['count']; ?></td>
                            <td class="text-nowrap">
                                <?= $row['isbn']; ?>
                                <?= ($isbn->validation->isbn($row['isbn']) == 1 ? "<i class=\"fas fa-check text-success\"></i>" : "<i class=\"fas fa-times text-danger\"></i>"); ?>
                            </td>
                            <td><?= $row['title']; ?></td>
                            <td><?= $row['faculty']; ?></td>
                            <td class="text-nowrap"><?= number_format($row['price'], 2, ",", " "); ?> &euro;</td>
                            <td><?= $new; ?></td>
                            <td class="text-nowrap">
                                <?= date_format(date_create($row['added']), "d.m.Y H:i"); ?> Uhr<br />
                                von <?= $row['username']; ?>
                            </td>
<!--                            <td style="text-align: right;">
                                <div class="btn-group">
                                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#editEntry<?= $row['id']; ?>"><i class="fas fa-edit"></i></a>
                                </div>
                                <form action="<?= filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_URL); ?>" method="post">
                                    <div class="modal fade text-left" id="editEntry<?= $row['id']; ?>" tabindex="-1">
                                        <div class="modal-dialog modal-dialog-centered modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="title">Eintrag bearbeiten</h5>
                                                    <button type="button" class="close" data-dismiss="modal">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                                                    <button type="submit" class="btn btn-primary" name="edit">Speichern</button>
                                                    <button type="submit" class="btn btn-danger del" name="delete">Löschen</button>
                                                    <input type="hidden" name="id" value="<?= $row['id']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </td>-->
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
include './frames/footer.php';
