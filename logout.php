<?php 
// turn on output buffering 
ob_start (); 

//destroy session
session_start (); 
session_unset (); 
session_destroy (); 

header ("Location: index.php");
//flush (send) the output buffer and turn off output buffering
ob_end_flush ();