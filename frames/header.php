<?php
if (!isset($_SESSION)) {
    session_start();
}
include_once 'database/connect.php';
include_once 'inc/config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>ESRS |  <?= $title; ?></title>

        <!--favicon-->
        <link rel="shortcut icon" type="image/x-icon" href="img/logo_fav.ico">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!--custom css-->
        <link rel="stylesheet" href="css/custom.css">

        <!--datetimepicker-->
        <link rel="stylesheet" href="css/jquery.datetimepicker.css" />



        <!--Fontawesome-->
        <script src="https://kit.fontawesome.com/5742603da7.js"></script>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <!--datetimepicker-->
        <script src="js/jquery.datetimepicker.full.min.js"></script>

        <!--TinyMCE-->
        <!--https://www.tiny.cloud/-->
        <script src="https://cdn.tiny.cloud/1/iwehwdd7eoqre8jicysauvc65zb2br9pbdx0z4ahi2pk4puz/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        <script>
            tinymce.init({
                selector: '#content',
                plugins: 'lists',
                toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
                        'bullist numlist outdent indent | link image | print preview media fullpage | ' +
                        'forecolor backcolor emoticons | help',
                height: '80vh',
                menubar: false,
                language: 'de'
            });
        </script>

        <!--custom js-->
        <script type="text/javascript" src="js/custom.js"></script>  

    </head>

    <body>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php">
                    <img src="img/logo_fav.png" alt="ESRS" class="d-inline-block align-top" width="64" height="64" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarsExampleDefault"
                        aria-controls="navbarsExampleDefault" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                    <ul class="navbar-nav mr-auto text-center">
                        <?php
                        if (isset($_SESSION['iduser']) && $_SESSION['verified'] == 1 && $_SESSION['role'] > 1) {
                            ?>
                            <li class="nav-item <?= isset($navoverview) ? " active" : ""; ?>"><a class="nav-link" href="overview.php"><i class="fas fa-table fa-3x"></i><br />Übersicht</a></li>
                            <?php
                        }
                        ?>

                    </ul>
                    <?php
                    if (!isset($_SESSION['iduser'])) {
                        ?>
                        <form class="form-inline my-2 my-lg-0" action="login.php?url=<?= filter_input(INPUT_GET, "url"); ?>" method="post">
                            <input class="form-control form-control-lg mr-sm-2" type="text" placeholder="Benutzer" name="shortcut" aria-label="username">
                            <input class="form-control form-control-lg mr-sm-2" type="password" placeholder="Passwort" name="password" aria-label="password">
                            <button class="btn btn-outline-primary btn-lg" type="submit">
                                <span class="fa-stack">
                                    <i class="fas fa-sign-in-alt fa-stack-2x"></i>
                                </span>
                            </button>
                            <a class="btn btn-outline-secondary ml-2 btn-lg" href="resetpassword.php">
                                <span class="fa-stack">
                                    <i class="fas fa-key fa-stack-1x"></i>
                                    <i class="fas fa-ban fa-stack-2x" style="color:Tomato"></i>
                                </span>
                            </a>
                        </form>
                        <?php
                    }
                    if (isset($_SESSION['iduser'])) {
                        ?>
                        <form class="form-inline my-2 my-lg-0" action="logout.php" method="post">
                            <span class="text-light mr-2"><?= date("d.m.Y"); ?></span>
                            <span id="clock" class="text-light mr-2"></span>
                            <button class="btn btn-outline-primary btn-lg" type="submit">
                                <span class="fa-stack">
                                    <i class="fas fa-sign-out-alt fa-stack-2x"></i> 
                                </span>
                                <strong><?= $_SESSION['username']; ?></strong>
                            </button>
                        </form>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </nav>