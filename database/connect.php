<?php

class foo_mysqli extends mysqli
{

    public function __construct($host, $user, $pass, $db)
    {
        parent::__construct($host, $user, $pass, $db);
        
        if (mysqli_connect_error()) {
            die('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }
    }
}

$db_schoolprogram = new foo_mysqli('localhost', '3srs_sch00lpr0gram', 'N".+hC3jetME:UK;', 'edith_stein_schoolprogram_db');

if (! $db_schoolprogram->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $mysqli->error);
} else {
    $db_schoolprogram->character_set_name();
}

$db_user = new foo_mysqli('localhost', '3srs_p0rtal', 'oy[u)1]RYzTo+3WT', 'edith_stein_user_db');

if (! $db_user->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $mysqli->error);
} else {
    $db_user->character_set_name();
}