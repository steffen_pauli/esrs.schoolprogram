// confirm delete
$(document).ready(function () {
    $(".del").click(function () {
        if (!confirm("Wirklich löschen? Die Aktion kann nicht rückgängig gemacht werden!")) {
            return false;
        }
    });
});

// confirm delete TOC item
$(document).ready(function () {
    $(".delTOC").click(function () {
        if (!confirm("Die Aktion kann nicht rückgängig gemacht werden. Ich habe die Hinweise zum Löschen des Eintrages (rot hinterlegter Bereich oberhalb der Tabelle) verstanden.")) {
            return false;
        }
    });
});

// initialize popover
$(function () {
    $('[data-toggle="popover"]').popover();
});

// initialize tooltip
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

