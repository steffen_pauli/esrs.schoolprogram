<?php

session_start();
include_once 'database/connect.php';

$shortcut = htmlspecialchars($_REQUEST['shortcut']);
$password = htmlspecialchars($_REQUEST['password']);

//search data from db
$query = "SELECT * FROM edith_stein_user_db.user WHERE username = '" . $shortcut . "';";
$result = $db_user->query($query);

//create array with user data
$row = $result->fetch_array(MYSQLI_ASSOC);

//check password
if (crypt($password, $row['password']) === $row['password']) {

    //write data to session array
    $_SESSION['iduser'] = $row['iduser'];
    $_SESSION['guid'] = $row['guid'];
    $_SESSION['id_school'] = $row['id_school'];
    $_SESSION['username'] = $row['username'];
    $_SESSION['verified'] = $row['verified'];
    $_SESSION['role'] = $row['role'];

//    $query = "UPDATE `edith_stein_book_db`.`user` SET `lastlogin`='" . date("Y-m-d H:i:s") . "' WHERE `guid`='" . $row['guid'] . "';";
//    $db_book->query($query);
    
    $url = "index.php";
    header("Location:$url");

    //login data not found
} else {
    header("Location:index.php?message=Logindaten unbekannt.");
}